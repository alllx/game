(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var _screenManager = require('./managers/screenManager');

var _screenManager2 = _interopRequireDefault(_screenManager);

var _assetsManager = require('./managers/assetsManager');

var _assetsManager2 = _interopRequireDefault(_assetsManager);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var canvas = document.querySelector('#game-stage');
var stage = new createjs.Stage(canvas);

_assetsManager2.default.load(function () {
  _screenManager2.default.init(stage);
  _screenManager2.default.change('StartScreen');
  stage.update();
});

},{"./managers/assetsManager":4,"./managers/screenManager":5}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _assetsManager = require('../managers/assetsManager');

var _assetsManager2 = _interopRequireDefault(_assetsManager);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Hero = function (_createjs$Sprite) {
  _inherits(Hero, _createjs$Sprite);

  function Hero(queue) {
    _classCallCheck(this, Hero);

    var ss = new createjs.SpriteSheet({
      images: [_assetsManager2.default.getResult('hero')],
      frames: { width: 100, height: 78, spacing: 4 },
      animations: {
        fly: 0,
        flap: [1, 3, 'fly'],
        dead: 4
      }
    });

    var _this = _possibleConstructorReturn(this, (Hero.__proto__ || Object.getPrototypeOf(Hero)).call(this, ss, 'fly'));

    _this.bounds = _this.getBounds();
    _this.regX = _this.bounds.width / 2;
    _this.regY = _this.bounds.height / 2;

    _this.dead = false;
    _this.vY = 0;
    _this.a = 450;
    _this.flapA = -350;
    return _this;
  }

  _createClass(Hero, [{
    key: 'move',
    value: function move(time) {
      this.y += this.vY * time + this.a * time * time / 2;
      this.vY += this.a * time;
    }
  }, {
    key: 'flap',
    value: function flap() {
      if (this.dead) {
        return;
      }
      this.vY += this.flapA;
      if (this.vY < this.flapA) {
        this.vY = this.flapA;
      }
      this.gotoAndPlay('flap');
    }
  }, {
    key: 'die',
    value: function die() {
      this.dead = true;
      this.gotoAndStop('dead');
      this.rotation = 20;
    }
  }]);

  return Hero;
}(createjs.Sprite);

exports.default = Hero;

},{"../managers/assetsManager":4}],3:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _assetsManager = require('../managers/assetsManager');

var _assetsManager2 = _interopRequireDefault(_assetsManager);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Spike = function (_createjs$Bitmap) {
  _inherits(Spike, _createjs$Bitmap);

  function Spike(queue) {
    _classCallCheck(this, Spike);

    var _this = _possibleConstructorReturn(this, (Spike.__proto__ || Object.getPrototypeOf(Spike)).call(this, _assetsManager2.default.getResult('spike')));

    _this.bounds = _this.getBounds();
    _this.regY = _this.bounds.height;
    _this.regX = _this.bounds.width / 2;
    return _this;
  }

  return Spike;
}(createjs.Bitmap);

exports.default = Spike;

},{"../managers/assetsManager":4}],4:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var manifest = [{ id: 'spike', src: 'img/spike.png' }, { id: 'hero', src: 'img/monster-sprite.png' }];

var assetsManager = {
  load: function load(callback) {
    this.queue = new createjs.LoadQueue();
    this.queue.loadManifest(manifest);
    this.queue.addEventListener('complete', callback);
  },
  getResult: function getResult(name) {
    return this.queue.getResult(name);
  }
};

exports.default = assetsManager;

},{}],5:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _StartScreen = require('../screens/StartScreen');

var _StartScreen2 = _interopRequireDefault(_StartScreen);

var _MainScreen = require('../screens/MainScreen');

var _MainScreen2 = _interopRequireDefault(_MainScreen);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var screenManager = {
  init: function init(stage) {
    var _this = this;

    this.stage = stage;
    this.currentScreen = null;
    this.screens = {
      StartScreen: _StartScreen2.default,
      MainScreen: _MainScreen2.default
    };

    createjs.Ticker.timingMode = createjs.Ticker.RAF;
    createjs.Ticker.addEventListener('tick', function (e) {
      if (_this.currentScreen && _this.currentScreen.tick) {
        _this.currentScreen.tick(e);
      }
      stage.update(e);
    });
  },
  change: function change(name) {
    if (this.currentScreen) {
      if (this.currentScreen.destroy) {
        this.currentScreen.destroy();
      }
      this.stage.removeChild(this.currentScreen);
    }
    this.currentScreen = new this.screens[name](this.stage.canvas.width, this.stage.canvas.height);
    this.stage.addChild(this.currentScreen);
  }
};

exports.default = screenManager;

},{"../screens/MainScreen":6,"../screens/StartScreen":7}],6:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Hero = require('../display/Hero');

var _Hero2 = _interopRequireDefault(_Hero);

var _Spike = require('../display/Spike');

var _Spike2 = _interopRequireDefault(_Spike);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// px per second
var speed = 300;

var hero = void 0;
var spikes = void 0;

var bgPosSky = 0;
var bgPosMountain = 0;
var bgPosGround = 0;
var groundHeight = 82;

var canvas = {};

var MainScreen = function (_createjs$Container) {
  _inherits(MainScreen, _createjs$Container);

  function MainScreen(width, height) {
    _classCallCheck(this, MainScreen);

    var _this = _possibleConstructorReturn(this, (MainScreen.__proto__ || Object.getPrototypeOf(MainScreen)).call(this));

    canvas.width = width;
    canvas.height = height;

    _this.hero = new _Hero2.default();
    _this.hero.x = canvas.width / 2;
    _this.hero.y = canvas.height / 2;

    _this.spikes = [new _Spike2.default(), new _Spike2.default()];
    _this.spikes.forEach(function (spike) {
      return _this.setSpike(spike);
    });
    _this.spikes[1].x += (canvas.width + _this.spikes[1].bounds.width) / 2;

    _this.addChild.apply(_this, _toConsumableArray(_this.spikes).concat([_this.hero]));

    // createjs.Ticker.timingMode = createjs.Ticker.RAF;
    // createjs.Ticker.addEventListener('tick', onTick);

    window.addEventListener('keydown', function () {
      return _this.hero.flap();
    });
    return _this;
  }

  _createClass(MainScreen, [{
    key: 'setSpike',
    value: function setSpike(spike) {
      spike.scaleY = Math.random() * 0.6 + 0.6;

      if (Math.random() < 0.5) {
        spike.y = 0;
        spike.rotation = 180;
      } else {
        spike.y = canvas.height - groundHeight;
        spike.rotation = 0;
      }
      spike.x = canvas.width + spike.bounds.width / 2;
    }
  }, {
    key: 'moveSpikes',
    value: function moveSpikes(time) {
      var _this2 = this;

      this.spikes.forEach(function (spike) {
        spike.x -= speed * time;

        if (spike.x <= -spike.bounds.width / 2) {
          _this2.setSpike(spike);
        }

        if (ndgmr.checkPixelCollision(spike, _this2.hero)) {
          _this2.hero.die();
        }
      });
    }
  }, {
    key: 'moveHero',
    value: function moveHero(time) {
      this.hero.move(time);
      if (this.hero.y < 0) {
        this.hero.y = 0;
        this.hero.vY = 0;
      } else if (this.hero.y > this.height - (this.groundHeight + this.hero.bounds.height / 2)) {
        this.hero.die();
      }
    }
  }, {
    key: 'tick',
    value: function tick(e) {
      var time = e.delta * 0.001;
      if (this.hero.y > this.height + this.hero.bounds.height || time * speed > 50) {
        return;
      }

      this.moveSpikes(time);
      this.moveHero(time);
    }
  }]);

  return MainScreen;
}(createjs.Container);

exports.default = MainScreen;

},{"../display/Hero":2,"../display/Spike":3}],7:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _screenManager = require('../managers/screenManager');

var _screenManager2 = _interopRequireDefault(_screenManager);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var StartScreen = function (_createjs$Container) {
  _inherits(StartScreen, _createjs$Container);

  function StartScreen() {
    _classCallCheck(this, StartScreen);

    var _this = _possibleConstructorReturn(this, (StartScreen.__proto__ || Object.getPrototypeOf(StartScreen)).call(this));

    var hello = new createjs.Text('hello world', '45px Arial', '#000');
    _this.addChild(hello);
    window.addEventListener('keyup', _this.onKeyUp);
    return _this;
  }

  _createClass(StartScreen, [{
    key: 'onKeyUp',
    value: function onKeyUp(e) {
      console.log('keyup');
      if (e.keyCode === 32) {
        _screenManager2.default.change('MainScreen');
      }
    }
  }, {
    key: 'destroy',
    value: function destroy() {
      window.removeEventListener('keyup', this.onKeyUp);
    }
  }]);

  return StartScreen;
}(createjs.Container);

exports.default = StartScreen;

},{"../managers/screenManager":5}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJhcHAvanMvc3JjL2FwcC5qcyIsImFwcC9qcy9zcmMvZGlzcGxheS9IZXJvLmpzIiwiYXBwL2pzL3NyYy9kaXNwbGF5L1NwaWtlLmpzIiwiYXBwL2pzL3NyYy9tYW5hZ2Vycy9hc3NldHNNYW5hZ2VyLmpzIiwiYXBwL2pzL3NyYy9tYW5hZ2Vycy9zY3JlZW5NYW5hZ2VyLmpzIiwiYXBwL2pzL3NyYy9zY3JlZW5zL01haW5TY3JlZW4uanMiLCJhcHAvanMvc3JjL3NjcmVlbnMvU3RhcnRTY3JlZW4uanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7OztBQ0FBOzs7O0FBQ0E7Ozs7OztBQUVBLElBQU0sU0FBUyxTQUFTLGFBQVQsQ0FBdUIsYUFBdkIsQ0FBZjtBQUNBLElBQU0sUUFBUSxJQUFJLFNBQVMsS0FBYixDQUFtQixNQUFuQixDQUFkOztBQUVBLHdCQUFjLElBQWQsQ0FBbUIsWUFBTTtBQUN2QiwwQkFBYyxJQUFkLENBQW1CLEtBQW5CO0FBQ0EsMEJBQWMsTUFBZCxDQUFxQixhQUFyQjtBQUNBLFFBQU0sTUFBTjtBQUNELENBSkQ7Ozs7Ozs7Ozs7O0FDTkE7Ozs7Ozs7Ozs7OztJQUVxQixJOzs7QUFDbkIsZ0JBQVksS0FBWixFQUFtQjtBQUFBOztBQUNqQixRQUFNLEtBQUssSUFBSSxTQUFTLFdBQWIsQ0FBeUI7QUFDbEMsY0FBUSxDQUFDLHdCQUFjLFNBQWQsQ0FBd0IsTUFBeEIsQ0FBRCxDQUQwQjtBQUVsQyxjQUFRLEVBQUUsT0FBTyxHQUFULEVBQWMsUUFBUSxFQUF0QixFQUEwQixTQUFTLENBQW5DLEVBRjBCO0FBR2xDLGtCQUFZO0FBQ1YsYUFBSyxDQURLO0FBRVYsY0FBTSxDQUFDLENBQUQsRUFBSSxDQUFKLEVBQU8sS0FBUCxDQUZJO0FBR1YsY0FBTTtBQUhJO0FBSHNCLEtBQXpCLENBQVg7O0FBRGlCLDRHQVVYLEVBVlcsRUFVUCxLQVZPOztBQVlqQixVQUFLLE1BQUwsR0FBYyxNQUFLLFNBQUwsRUFBZDtBQUNBLFVBQUssSUFBTCxHQUFZLE1BQUssTUFBTCxDQUFZLEtBQVosR0FBb0IsQ0FBaEM7QUFDQSxVQUFLLElBQUwsR0FBWSxNQUFLLE1BQUwsQ0FBWSxNQUFaLEdBQXFCLENBQWpDOztBQUVBLFVBQUssSUFBTCxHQUFZLEtBQVo7QUFDQSxVQUFLLEVBQUwsR0FBVSxDQUFWO0FBQ0EsVUFBSyxDQUFMLEdBQVMsR0FBVDtBQUNBLFVBQUssS0FBTCxHQUFhLENBQUMsR0FBZDtBQW5CaUI7QUFvQmxCOzs7O3lCQUNJLEksRUFBTTtBQUNULFdBQUssQ0FBTCxJQUFXLEtBQUssRUFBTCxHQUFVLElBQVgsR0FBb0IsS0FBSyxDQUFMLEdBQVMsSUFBVCxHQUFnQixJQUFqQixHQUF5QixDQUF0RDtBQUNBLFdBQUssRUFBTCxJQUFXLEtBQUssQ0FBTCxHQUFTLElBQXBCO0FBQ0Q7OzsyQkFDTTtBQUNMLFVBQUksS0FBSyxJQUFULEVBQWU7QUFDYjtBQUNEO0FBQ0QsV0FBSyxFQUFMLElBQVcsS0FBSyxLQUFoQjtBQUNBLFVBQUksS0FBSyxFQUFMLEdBQVUsS0FBSyxLQUFuQixFQUEwQjtBQUN4QixhQUFLLEVBQUwsR0FBVSxLQUFLLEtBQWY7QUFDRDtBQUNELFdBQUssV0FBTCxDQUFpQixNQUFqQjtBQUNEOzs7MEJBQ0s7QUFDSixXQUFLLElBQUwsR0FBWSxJQUFaO0FBQ0EsV0FBSyxXQUFMLENBQWlCLE1BQWpCO0FBQ0EsV0FBSyxRQUFMLEdBQWdCLEVBQWhCO0FBQ0Q7Ozs7RUF4QytCLFNBQVMsTTs7a0JBQXRCLEk7Ozs7Ozs7OztBQ0ZyQjs7Ozs7Ozs7Ozs7O0lBRXFCLEs7OztBQUNuQixpQkFBWSxLQUFaLEVBQW1CO0FBQUE7O0FBQUEsOEdBQ1gsd0JBQWMsU0FBZCxDQUF3QixPQUF4QixDQURXOztBQUdqQixVQUFLLE1BQUwsR0FBYyxNQUFLLFNBQUwsRUFBZDtBQUNBLFVBQUssSUFBTCxHQUFZLE1BQUssTUFBTCxDQUFZLE1BQXhCO0FBQ0EsVUFBSyxJQUFMLEdBQVksTUFBSyxNQUFMLENBQVksS0FBWixHQUFvQixDQUFoQztBQUxpQjtBQU1sQjs7O0VBUGdDLFNBQVMsTTs7a0JBQXZCLEs7Ozs7Ozs7O0FDRnJCLElBQU0sV0FBVyxDQUNYLEVBQUUsSUFBSSxPQUFOLEVBQWUsS0FBSyxlQUFwQixFQURXLEVBRVgsRUFBRSxJQUFJLE1BQU4sRUFBYyxLQUFLLHdCQUFuQixFQUZXLENBQWpCOztBQUtBLElBQU0sZ0JBQWdCO0FBQ3BCLE1BRG9CLGdCQUNmLFFBRGUsRUFDTDtBQUNiLFNBQUssS0FBTCxHQUFhLElBQUksU0FBUyxTQUFiLEVBQWI7QUFDQSxTQUFLLEtBQUwsQ0FBVyxZQUFYLENBQXdCLFFBQXhCO0FBQ0EsU0FBSyxLQUFMLENBQVcsZ0JBQVgsQ0FBNEIsVUFBNUIsRUFBd0MsUUFBeEM7QUFDRCxHQUxtQjtBQU1wQixXQU5vQixxQkFNVixJQU5VLEVBTUo7QUFDZixXQUFPLEtBQUssS0FBTCxDQUFXLFNBQVgsQ0FBcUIsSUFBckIsQ0FBUDtBQUNEO0FBUm9CLENBQXRCOztrQkFXZSxhOzs7Ozs7Ozs7QUNoQmY7Ozs7QUFDQTs7Ozs7O0FBRUEsSUFBTSxnQkFBZ0I7QUFDcEIsTUFEb0IsZ0JBQ2YsS0FEZSxFQUNSO0FBQUE7O0FBQ1YsU0FBSyxLQUFMLEdBQWEsS0FBYjtBQUNBLFNBQUssYUFBTCxHQUFxQixJQUFyQjtBQUNBLFNBQUssT0FBTCxHQUFlO0FBQ2Isd0NBRGE7QUFFYjtBQUZhLEtBQWY7O0FBS0EsYUFBUyxNQUFULENBQWdCLFVBQWhCLEdBQTZCLFNBQVMsTUFBVCxDQUFnQixHQUE3QztBQUNBLGFBQVMsTUFBVCxDQUFnQixnQkFBaEIsQ0FBaUMsTUFBakMsRUFBeUMsYUFBSztBQUM1QyxVQUFJLE1BQUssYUFBTCxJQUFzQixNQUFLLGFBQUwsQ0FBbUIsSUFBN0MsRUFBbUQ7QUFDakQsY0FBSyxhQUFMLENBQW1CLElBQW5CLENBQXdCLENBQXhCO0FBQ0Q7QUFDRCxZQUFNLE1BQU4sQ0FBYSxDQUFiO0FBQ0QsS0FMRDtBQU1ELEdBaEJtQjtBQWlCcEIsUUFqQm9CLGtCQWlCYixJQWpCYSxFQWlCUDtBQUNYLFFBQUksS0FBSyxhQUFULEVBQXdCO0FBQ3RCLFVBQUksS0FBSyxhQUFMLENBQW1CLE9BQXZCLEVBQWdDO0FBQzlCLGFBQUssYUFBTCxDQUFtQixPQUFuQjtBQUNEO0FBQ0QsV0FBSyxLQUFMLENBQVcsV0FBWCxDQUF1QixLQUFLLGFBQTVCO0FBQ0Q7QUFDRCxTQUFLLGFBQUwsR0FBcUIsSUFBSSxLQUFLLE9BQUwsQ0FBYSxJQUFiLENBQUosQ0FDakIsS0FBSyxLQUFMLENBQVcsTUFBWCxDQUFrQixLQURELEVBRWpCLEtBQUssS0FBTCxDQUFXLE1BQVgsQ0FBa0IsTUFGRCxDQUFyQjtBQUlBLFNBQUssS0FBTCxDQUFXLFFBQVgsQ0FBb0IsS0FBSyxhQUF6QjtBQUNEO0FBN0JtQixDQUF0Qjs7a0JBZ0NlLGE7Ozs7Ozs7Ozs7O0FDbkNmOzs7O0FBQ0E7Ozs7Ozs7Ozs7Ozs7O0FBRUE7QUFDQSxJQUFNLFFBQVEsR0FBZDs7QUFFQSxJQUFJLGFBQUo7QUFDQSxJQUFJLGVBQUo7O0FBRUEsSUFBSSxXQUFXLENBQWY7QUFDQSxJQUFJLGdCQUFnQixDQUFwQjtBQUNBLElBQUksY0FBYyxDQUFsQjtBQUNBLElBQU0sZUFBZSxFQUFyQjs7QUFFQSxJQUFNLFNBQVMsRUFBZjs7SUFFcUIsVTs7O0FBQ25CLHNCQUFZLEtBQVosRUFBbUIsTUFBbkIsRUFBMkI7QUFBQTs7QUFBQTs7QUFFekIsV0FBTyxLQUFQLEdBQWUsS0FBZjtBQUNBLFdBQU8sTUFBUCxHQUFnQixNQUFoQjs7QUFHRCxVQUFLLElBQUwsR0FBWSxvQkFBWjtBQUNBLFVBQUssSUFBTCxDQUFVLENBQVYsR0FBYyxPQUFPLEtBQVAsR0FBZSxDQUE3QjtBQUNBLFVBQUssSUFBTCxDQUFVLENBQVYsR0FBYyxPQUFPLE1BQVAsR0FBZ0IsQ0FBOUI7O0FBRUEsVUFBSyxNQUFMLEdBQWMsQ0FBQyxxQkFBRCxFQUFjLHFCQUFkLENBQWQ7QUFDQSxVQUFLLE1BQUwsQ0FBWSxPQUFaLENBQW9CO0FBQUEsYUFBUyxNQUFLLFFBQUwsQ0FBYyxLQUFkLENBQVQ7QUFBQSxLQUFwQjtBQUNBLFVBQUssTUFBTCxDQUFZLENBQVosRUFBZSxDQUFmLElBQW9CLENBQUMsT0FBTyxLQUFQLEdBQWUsTUFBSyxNQUFMLENBQVksQ0FBWixFQUFlLE1BQWYsQ0FBc0IsS0FBdEMsSUFBK0MsQ0FBbkU7O0FBRUEsVUFBSyxRQUFMLGlDQUFpQixNQUFLLE1BQXRCLFVBQThCLE1BQUssSUFBbkM7O0FBRUE7QUFDQTs7QUFFQSxXQUFPLGdCQUFQLENBQXdCLFNBQXhCLEVBQW1DO0FBQUEsYUFBTSxNQUFLLElBQUwsQ0FBVSxJQUFWLEVBQU47QUFBQSxLQUFuQztBQW5CMEI7QUFvQjFCOzs7OzZCQUNPLEssRUFBTztBQUNmLFlBQU0sTUFBTixHQUFnQixLQUFLLE1BQUwsS0FBZ0IsR0FBakIsR0FBd0IsR0FBdkM7O0FBRUEsVUFBSSxLQUFLLE1BQUwsS0FBZ0IsR0FBcEIsRUFBeUI7QUFDdkIsY0FBTSxDQUFOLEdBQVUsQ0FBVjtBQUNBLGNBQU0sUUFBTixHQUFpQixHQUFqQjtBQUNELE9BSEQsTUFHTztBQUNMLGNBQU0sQ0FBTixHQUFVLE9BQU8sTUFBUCxHQUFnQixZQUExQjtBQUNBLGNBQU0sUUFBTixHQUFpQixDQUFqQjtBQUNEO0FBQ0QsWUFBTSxDQUFOLEdBQVUsT0FBTyxLQUFQLEdBQWdCLE1BQU0sTUFBTixDQUFhLEtBQWIsR0FBcUIsQ0FBL0M7QUFDRDs7OytCQUVXLEksRUFBTTtBQUFBOztBQUNoQixXQUFLLE1BQUwsQ0FBWSxPQUFaLENBQW9CLGlCQUFTO0FBQzNCLGNBQU0sQ0FBTixJQUFXLFFBQVEsSUFBbkI7O0FBRUEsWUFBSSxNQUFNLENBQU4sSUFBVyxDQUFDLE1BQU0sTUFBTixDQUFhLEtBQWQsR0FBc0IsQ0FBckMsRUFBd0M7QUFDdEMsaUJBQUssUUFBTCxDQUFjLEtBQWQ7QUFDRDs7QUFFRCxZQUFJLE1BQU0sbUJBQU4sQ0FBMEIsS0FBMUIsRUFBaUMsT0FBSyxJQUF0QyxDQUFKLEVBQWlEO0FBQy9DLGlCQUFLLElBQUwsQ0FBVSxHQUFWO0FBQ0Q7QUFDRixPQVZEO0FBV0Q7Ozs2QkFFUyxJLEVBQU07QUFDZCxXQUFLLElBQUwsQ0FBVSxJQUFWLENBQWUsSUFBZjtBQUNBLFVBQUksS0FBSyxJQUFMLENBQVUsQ0FBVixHQUFjLENBQWxCLEVBQXFCO0FBQ25CLGFBQUssSUFBTCxDQUFVLENBQVYsR0FBYyxDQUFkO0FBQ0EsYUFBSyxJQUFMLENBQVUsRUFBVixHQUFlLENBQWY7QUFDRCxPQUhELE1BR08sSUFBSSxLQUFLLElBQUwsQ0FBVSxDQUFWLEdBQWMsS0FBSyxNQUFMLElBQWUsS0FBSyxZQUFMLEdBQW9CLEtBQUssSUFBTCxDQUFVLE1BQVYsQ0FBaUIsTUFBakIsR0FBMEIsQ0FBN0QsQ0FBbEIsRUFBbUY7QUFDeEYsYUFBSyxJQUFMLENBQVUsR0FBVjtBQUNEO0FBQ0Y7Ozt5QkFFSyxDLEVBQUc7QUFDUCxVQUFNLE9BQU8sRUFBRSxLQUFGLEdBQVUsS0FBdkI7QUFDQSxVQUFJLEtBQUssSUFBTCxDQUFVLENBQVYsR0FBYyxLQUFLLE1BQUwsR0FBYyxLQUFLLElBQUwsQ0FBVSxNQUFWLENBQWlCLE1BQTdDLElBQ0YsT0FBTyxLQUFQLEdBQWUsRUFEakIsRUFDcUI7QUFDbkI7QUFDRDs7QUFFRCxXQUFLLFVBQUwsQ0FBZ0IsSUFBaEI7QUFDQSxXQUFLLFFBQUwsQ0FBYyxJQUFkO0FBRUQ7Ozs7RUFyRXVDLFNBQVMsUzs7a0JBQTVCLFU7Ozs7Ozs7Ozs7O0FDaEJyQjs7Ozs7Ozs7Ozs7O0lBRXFCLFc7OztBQUNuQix5QkFBYztBQUFBOztBQUFBOztBQUdaLFFBQU0sUUFBUSxJQUFJLFNBQVMsSUFBYixDQUFrQixhQUFsQixFQUNrQixZQURsQixFQUVrQixNQUZsQixDQUFkO0FBR0EsVUFBSyxRQUFMLENBQWMsS0FBZDtBQUNBLFdBQU8sZ0JBQVAsQ0FBd0IsT0FBeEIsRUFBaUMsTUFBSyxPQUF0QztBQVBZO0FBUWI7Ozs7NEJBQ08sQyxFQUFFO0FBQ1IsY0FBUSxHQUFSLENBQVksT0FBWjtBQUNBLFVBQUksRUFBRSxPQUFGLEtBQWMsRUFBbEIsRUFBcUI7QUFDbkIsZ0NBQWMsTUFBZCxDQUFxQixZQUFyQjtBQUNIO0FBQ0Y7Ozs4QkFDVTtBQUNQLGFBQU8sbUJBQVAsQ0FBMkIsT0FBM0IsRUFBb0MsS0FBSyxPQUF6QztBQUNEOzs7O0VBbEJzQyxTQUFTLFM7O2tCQUE3QixXIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsImltcG9ydCBzY3JlZW5NYW5hZ2VyIGZyb20gJy4vbWFuYWdlcnMvc2NyZWVuTWFuYWdlcic7XG5pbXBvcnQgYXNzZXRzTWFuYWdlciBmcm9tICcuL21hbmFnZXJzL2Fzc2V0c01hbmFnZXInO1xuXG5jb25zdCBjYW52YXMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjZ2FtZS1zdGFnZScpO1xuY29uc3Qgc3RhZ2UgPSBuZXcgY3JlYXRlanMuU3RhZ2UoY2FudmFzKTtcblxuYXNzZXRzTWFuYWdlci5sb2FkKCgpID0+IHtcbiAgc2NyZWVuTWFuYWdlci5pbml0KHN0YWdlKTtcbiAgc2NyZWVuTWFuYWdlci5jaGFuZ2UoJ1N0YXJ0U2NyZWVuJyk7XG4gIHN0YWdlLnVwZGF0ZSgpO1xufSk7XG4iLCJpbXBvcnQgYXNzZXRzTWFuYWdlciBmcm9tICcuLi9tYW5hZ2Vycy9hc3NldHNNYW5hZ2VyJztcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgSGVybyBleHRlbmRzIGNyZWF0ZWpzLlNwcml0ZSB7XG4gIGNvbnN0cnVjdG9yKHF1ZXVlKSB7XG4gICAgY29uc3Qgc3MgPSBuZXcgY3JlYXRlanMuU3ByaXRlU2hlZXQoe1xuICAgICAgaW1hZ2VzOiBbYXNzZXRzTWFuYWdlci5nZXRSZXN1bHQoJ2hlcm8nKV0sXG4gICAgICBmcmFtZXM6IHsgd2lkdGg6IDEwMCwgaGVpZ2h0OiA3OCwgc3BhY2luZzogNCB9LFxuICAgICAgYW5pbWF0aW9uczoge1xuICAgICAgICBmbHk6IDAsXG4gICAgICAgIGZsYXA6IFsxLCAzLCAnZmx5J10sXG4gICAgICAgIGRlYWQ6IDQsXG4gICAgICB9LFxuICAgIH0pO1xuICAgIHN1cGVyKHNzLCAnZmx5Jyk7XG5cbiAgICB0aGlzLmJvdW5kcyA9IHRoaXMuZ2V0Qm91bmRzKCk7XG4gICAgdGhpcy5yZWdYID0gdGhpcy5ib3VuZHMud2lkdGggLyAyO1xuICAgIHRoaXMucmVnWSA9IHRoaXMuYm91bmRzLmhlaWdodCAvIDI7XG5cbiAgICB0aGlzLmRlYWQgPSBmYWxzZTtcbiAgICB0aGlzLnZZID0gMDtcbiAgICB0aGlzLmEgPSA0NTA7XG4gICAgdGhpcy5mbGFwQSA9IC0zNTA7XG4gIH1cbiAgbW92ZSh0aW1lKSB7XG4gICAgdGhpcy55ICs9ICh0aGlzLnZZICogdGltZSkgKyAodGhpcy5hICogdGltZSAqIHRpbWUpIC8gMjtcbiAgICB0aGlzLnZZICs9IHRoaXMuYSAqIHRpbWU7XG4gIH1cbiAgZmxhcCgpIHtcbiAgICBpZiAodGhpcy5kZWFkKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIHRoaXMudlkgKz0gdGhpcy5mbGFwQTtcbiAgICBpZiAodGhpcy52WSA8IHRoaXMuZmxhcEEpIHtcbiAgICAgIHRoaXMudlkgPSB0aGlzLmZsYXBBO1xuICAgIH1cbiAgICB0aGlzLmdvdG9BbmRQbGF5KCdmbGFwJyk7XG4gIH1cbiAgZGllKCkge1xuICAgIHRoaXMuZGVhZCA9IHRydWU7XG4gICAgdGhpcy5nb3RvQW5kU3RvcCgnZGVhZCcpO1xuICAgIHRoaXMucm90YXRpb24gPSAyMDtcbiAgfVxufVxuIiwiaW1wb3J0IGFzc2V0c01hbmFnZXIgZnJvbSAnLi4vbWFuYWdlcnMvYXNzZXRzTWFuYWdlcic7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNwaWtlIGV4dGVuZHMgY3JlYXRlanMuQml0bWFwIHtcbiAgY29uc3RydWN0b3IocXVldWUpIHtcbiAgICBzdXBlcihhc3NldHNNYW5hZ2VyLmdldFJlc3VsdCgnc3Bpa2UnKSk7XG5cbiAgICB0aGlzLmJvdW5kcyA9IHRoaXMuZ2V0Qm91bmRzKCk7XG4gICAgdGhpcy5yZWdZID0gdGhpcy5ib3VuZHMuaGVpZ2h0O1xuICAgIHRoaXMucmVnWCA9IHRoaXMuYm91bmRzLndpZHRoIC8gMjtcbiAgfVxufVxuIiwiY29uc3QgbWFuaWZlc3QgPSBbXG4gICAgICB7IGlkOiAnc3Bpa2UnLCBzcmM6ICdpbWcvc3Bpa2UucG5nJyB9LFxuICAgICAgeyBpZDogJ2hlcm8nLCBzcmM6ICdpbWcvbW9uc3Rlci1zcHJpdGUucG5nJyB9LFxuXTtcblxuY29uc3QgYXNzZXRzTWFuYWdlciA9IHtcbiAgbG9hZChjYWxsYmFjaykge1xuICAgIHRoaXMucXVldWUgPSBuZXcgY3JlYXRlanMuTG9hZFF1ZXVlKCk7XG4gICAgdGhpcy5xdWV1ZS5sb2FkTWFuaWZlc3QobWFuaWZlc3QpO1xuICAgIHRoaXMucXVldWUuYWRkRXZlbnRMaXN0ZW5lcignY29tcGxldGUnLCBjYWxsYmFjayk7XG4gIH0sXG4gIGdldFJlc3VsdChuYW1lKSB7XG5cdFx0IHJldHVyblx0dGhpcy5xdWV1ZS5nZXRSZXN1bHQobmFtZSk7XG5cdH0sXG59O1xuXG5leHBvcnQgZGVmYXVsdCBhc3NldHNNYW5hZ2VyOyIsImltcG9ydCBTdGFydFNjcmVlbiBmcm9tICcuLi9zY3JlZW5zL1N0YXJ0U2NyZWVuJztcbmltcG9ydCBNYWluU2NyZWVuIGZyb20gJy4uL3NjcmVlbnMvTWFpblNjcmVlbic7XG5cbmNvbnN0IHNjcmVlbk1hbmFnZXIgPSB7XG4gIGluaXQoc3RhZ2UpIHtcbiAgICB0aGlzLnN0YWdlID0gc3RhZ2U7XG4gICAgdGhpcy5jdXJyZW50U2NyZWVuID0gbnVsbDtcbiAgICB0aGlzLnNjcmVlbnMgPSB7XG4gICAgICBTdGFydFNjcmVlbixcbiAgICAgIE1haW5TY3JlZW4sXG4gICAgfTtcblxuICAgIGNyZWF0ZWpzLlRpY2tlci50aW1pbmdNb2RlID0gY3JlYXRlanMuVGlja2VyLlJBRjtcbiAgICBjcmVhdGVqcy5UaWNrZXIuYWRkRXZlbnRMaXN0ZW5lcigndGljaycsIGUgPT4ge1xuICAgICAgaWYgKHRoaXMuY3VycmVudFNjcmVlbiAmJiB0aGlzLmN1cnJlbnRTY3JlZW4udGljaykge1xuICAgICAgICB0aGlzLmN1cnJlbnRTY3JlZW4udGljayhlKTtcbiAgICAgIH1cbiAgICAgIHN0YWdlLnVwZGF0ZShlKTtcbiAgICB9KTtcbiAgfSxcbiAgY2hhbmdlKG5hbWUpIHtcbiAgICBpZiAodGhpcy5jdXJyZW50U2NyZWVuKSB7XG4gICAgICBpZiAodGhpcy5jdXJyZW50U2NyZWVuLmRlc3Ryb3kpIHtcbiAgICAgICAgdGhpcy5jdXJyZW50U2NyZWVuLmRlc3Ryb3koKTtcbiAgICAgIH1cbiAgICAgIHRoaXMuc3RhZ2UucmVtb3ZlQ2hpbGQodGhpcy5jdXJyZW50U2NyZWVuKTtcbiAgICB9XG4gICAgdGhpcy5jdXJyZW50U2NyZWVuID0gbmV3IHRoaXMuc2NyZWVuc1tuYW1lXShcbiAgICAgICAgdGhpcy5zdGFnZS5jYW52YXMud2lkdGgsXG4gICAgICAgIHRoaXMuc3RhZ2UuY2FudmFzLmhlaWdodFxuICAgICAgKTtcbiAgICB0aGlzLnN0YWdlLmFkZENoaWxkKHRoaXMuY3VycmVudFNjcmVlbik7XG4gIH0sXG59O1xuXG5leHBvcnQgZGVmYXVsdCBzY3JlZW5NYW5hZ2VyO1xuIiwiaW1wb3J0IEhlcm8gZnJvbSAnLi4vZGlzcGxheS9IZXJvJztcbmltcG9ydCBTcGlrZSBmcm9tICcuLi9kaXNwbGF5L1NwaWtlJztcblxuLy8gcHggcGVyIHNlY29uZFxuY29uc3Qgc3BlZWQgPSAzMDA7XG5cbmxldCBoZXJvO1xubGV0IHNwaWtlcztcblxubGV0IGJnUG9zU2t5ID0gMDtcbmxldCBiZ1Bvc01vdW50YWluID0gMDtcbmxldCBiZ1Bvc0dyb3VuZCA9IDA7XG5jb25zdCBncm91bmRIZWlnaHQgPSA4MjtcblxuY29uc3QgY2FudmFzID0ge307XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE1haW5TY3JlZW4gZXh0ZW5kcyBjcmVhdGVqcy5Db250YWluZXIge1xuICBjb25zdHJ1Y3Rvcih3aWR0aCwgaGVpZ2h0KSB7XG4gICAgc3VwZXIoKTtcbiAgICBjYW52YXMud2lkdGggPSB3aWR0aDtcbiAgICBjYW52YXMuaGVpZ2h0ID0gaGVpZ2h0O1xuICAgXG5cbiAgIHRoaXMuaGVybyA9IG5ldyBIZXJvKCk7XG4gICB0aGlzLmhlcm8ueCA9IGNhbnZhcy53aWR0aCAvIDI7XG4gICB0aGlzLmhlcm8ueSA9IGNhbnZhcy5oZWlnaHQgLyAyO1xuXG4gICB0aGlzLnNwaWtlcyA9IFtuZXcgU3Bpa2UoKSwgbmV3IFNwaWtlKCldO1xuICAgdGhpcy5zcGlrZXMuZm9yRWFjaChzcGlrZSA9PiB0aGlzLnNldFNwaWtlKHNwaWtlKSk7XG4gICB0aGlzLnNwaWtlc1sxXS54ICs9IChjYW52YXMud2lkdGggKyB0aGlzLnNwaWtlc1sxXS5ib3VuZHMud2lkdGgpIC8gMjtcblxuICAgdGhpcy5hZGRDaGlsZCguLi50aGlzLnNwaWtlcywgdGhpcy5oZXJvKTtcblxuICAgLy8gY3JlYXRlanMuVGlja2VyLnRpbWluZ01vZGUgPSBjcmVhdGVqcy5UaWNrZXIuUkFGO1xuICAgLy8gY3JlYXRlanMuVGlja2VyLmFkZEV2ZW50TGlzdGVuZXIoJ3RpY2snLCBvblRpY2spO1xuXG4gICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigna2V5ZG93bicsICgpID0+IHRoaXMuaGVyby5mbGFwKCkpO1xuICB9XG4gc2V0U3Bpa2Uoc3Bpa2UpIHtcbiAgc3Bpa2Uuc2NhbGVZID0gKE1hdGgucmFuZG9tKCkgKiAwLjYpICsgMC42O1xuXG4gIGlmIChNYXRoLnJhbmRvbSgpIDwgMC41KSB7XG4gICAgc3Bpa2UueSA9IDA7XG4gICAgc3Bpa2Uucm90YXRpb24gPSAxODA7XG4gIH0gZWxzZSB7XG4gICAgc3Bpa2UueSA9IGNhbnZhcy5oZWlnaHQgLSBncm91bmRIZWlnaHQ7XG4gICAgc3Bpa2Uucm90YXRpb24gPSAwO1xuICB9XG4gIHNwaWtlLnggPSBjYW52YXMud2lkdGggKyAoc3Bpa2UuYm91bmRzLndpZHRoIC8gMik7XG59XG5cbiBtb3ZlU3Bpa2VzKHRpbWUpIHtcbiAgdGhpcy5zcGlrZXMuZm9yRWFjaChzcGlrZSA9PiB7XG4gICAgc3Bpa2UueCAtPSBzcGVlZCAqIHRpbWU7XG5cbiAgICBpZiAoc3Bpa2UueCA8PSAtc3Bpa2UuYm91bmRzLndpZHRoIC8gMikge1xuICAgICAgdGhpcy5zZXRTcGlrZShzcGlrZSk7XG4gICAgfVxuXG4gICAgaWYgKG5kZ21yLmNoZWNrUGl4ZWxDb2xsaXNpb24oc3Bpa2UsIHRoaXMuaGVybykpIHtcbiAgICAgIHRoaXMuaGVyby5kaWUoKTtcbiAgICB9XG4gIH0pO1xufVxuXG4gbW92ZUhlcm8odGltZSkge1xuICB0aGlzLmhlcm8ubW92ZSh0aW1lKTtcbiAgaWYgKHRoaXMuaGVyby55IDwgMCkge1xuICAgIHRoaXMuaGVyby55ID0gMDtcbiAgICB0aGlzLmhlcm8udlkgPSAwO1xuICB9IGVsc2UgaWYgKHRoaXMuaGVyby55ID4gdGhpcy5oZWlnaHQgLSAodGhpcy5ncm91bmRIZWlnaHQgKyB0aGlzLmhlcm8uYm91bmRzLmhlaWdodCAvIDIpKSB7XG4gICAgdGhpcy5oZXJvLmRpZSgpO1xuICB9XG59XG5cbiB0aWNrKGUpIHtcbiAgY29uc3QgdGltZSA9IGUuZGVsdGEgKiAwLjAwMTtcbiAgaWYgKHRoaXMuaGVyby55ID4gdGhpcy5oZWlnaHQgKyB0aGlzLmhlcm8uYm91bmRzLmhlaWdodCB8fFxuICAgIHRpbWUgKiBzcGVlZCA+IDUwKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdGhpcy5tb3ZlU3Bpa2VzKHRpbWUpO1xuICB0aGlzLm1vdmVIZXJvKHRpbWUpO1xuXG59XG59XG4iLCJpbXBvcnQgc2NyZWVuTWFuYWdlciBmcm9tICcuLi9tYW5hZ2Vycy9zY3JlZW5NYW5hZ2VyJztcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU3RhcnRTY3JlZW4gZXh0ZW5kcyBjcmVhdGVqcy5Db250YWluZXIge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBzdXBlcigpO1xuXG4gICAgY29uc3QgaGVsbG8gPSBuZXcgY3JlYXRlanMuVGV4dCgnaGVsbG8gd29ybGQnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJzQ1cHggQXJpYWwnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJyMwMDAnKTtcbiAgICB0aGlzLmFkZENoaWxkKGhlbGxvKTtcbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigna2V5dXAnLCB0aGlzLm9uS2V5VXApO1xuICB9XG4gIG9uS2V5VXAoZSl7XG4gICAgY29uc29sZS5sb2coJ2tleXVwJyk7XG4gICAgaWYgKGUua2V5Q29kZSA9PT0gMzIpe1xuICAgICAgc2NyZWVuTWFuYWdlci5jaGFuZ2UoJ01haW5TY3JlZW4nKTtcbiAgfVxufVxuICBkZXN0cm95KCl7XG4gICAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2tleXVwJywgdGhpcy5vbktleVVwKTtcbiAgfVxufVxuXG4iXX0=
