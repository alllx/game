import screenManager from '../managers/screenManager';

export default class StartScreen extends createjs.Container {
  constructor() {
    super();

    const hello = new createjs.Text('hello world',
                                    '45px Arial',
                                    '#000');
    this.addChild(hello);
    window.addEventListener('keyup', this.onKeyUp);
  }
  onKeyUp(e){
    console.log('keyup');
    if (e.keyCode === 32){
      screenManager.change('MainScreen');
  }
}
  destroy(){
    window.removeEventListener('keyup', this.onKeyUp);
  }
}

