import Hero from '../display/Hero';
import Spike from '../display/Spike';

// px per second
const speed = 300;

let hero;
let spikes;

let bgPosSky = 0;
let bgPosMountain = 0;
let bgPosGround = 0;
const groundHeight = 82;

const canvas = {};

export default class MainScreen extends createjs.Container {
  constructor(width, height) {
    super();
    canvas.width = width;
    canvas.height = height;
   

   this.hero = new Hero();
   this.hero.x = canvas.width / 2;
   this.hero.y = canvas.height / 2;

   this.spikes = [new Spike(), new Spike()];
   this.spikes.forEach(spike => this.setSpike(spike));
   this.spikes[1].x += (canvas.width + this.spikes[1].bounds.width) / 2;

   this.addChild(...this.spikes, this.hero);

   // createjs.Ticker.timingMode = createjs.Ticker.RAF;
   // createjs.Ticker.addEventListener('tick', onTick);

   window.addEventListener('keydown', () => this.hero.flap());
  }
 setSpike(spike) {
  spike.scaleY = (Math.random() * 0.6) + 0.6;

  if (Math.random() < 0.5) {
    spike.y = 0;
    spike.rotation = 180;
  } else {
    spike.y = canvas.height - groundHeight;
    spike.rotation = 0;
  }
  spike.x = canvas.width + (spike.bounds.width / 2);
}

 moveSpikes(time) {
  this.spikes.forEach(spike => {
    spike.x -= speed * time;

    if (spike.x <= -spike.bounds.width / 2) {
      this.setSpike(spike);
    }

    if (ndgmr.checkPixelCollision(spike, this.hero)) {
      this.hero.die();
    }
  });
}

 moveHero(time) {
  this.hero.move(time);
  if (this.hero.y < 0) {
    this.hero.y = 0;
    this.hero.vY = 0;
  } else if (this.hero.y > this.height - (this.groundHeight + this.hero.bounds.height / 2)) {
    this.hero.die();
  }
}

 tick(e) {
  const time = e.delta * 0.001;
  if (this.hero.y > this.height + this.hero.bounds.height ||
    time * speed > 50) {
    return;
  }

  this.moveSpikes(time);
  this.moveHero(time);

}
}
