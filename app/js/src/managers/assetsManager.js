const manifest = [
      { id: 'spike', src: 'img/spike.png' },
      { id: 'hero', src: 'img/monster-sprite.png' },
];

const assetsManager = {
  load(callback) {
    this.queue = new createjs.LoadQueue();
    this.queue.loadManifest(manifest);
    this.queue.addEventListener('complete', callback);
  },
  getResult(name) {
		 return	this.queue.getResult(name);
	},
};

export default assetsManager;