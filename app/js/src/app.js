import screenManager from './managers/screenManager';
import assetsManager from './managers/assetsManager';

const canvas = document.querySelector('#game-stage');
const stage = new createjs.Stage(canvas);

assetsManager.load(() => {
  screenManager.init(stage);
  screenManager.change('StartScreen');
  stage.update();
});
