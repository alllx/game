import assetsManager from '../managers/assetsManager';

export default class Hero extends createjs.Sprite {
  constructor(queue) {
    const ss = new createjs.SpriteSheet({
      images: [assetsManager.getResult('hero')],
      frames: { width: 100, height: 78, spacing: 4 },
      animations: {
        fly: 0,
        flap: [1, 3, 'fly'],
        dead: 4,
      },
    });
    super(ss, 'fly');

    this.bounds = this.getBounds();
    this.regX = this.bounds.width / 2;
    this.regY = this.bounds.height / 2;

    this.dead = false;
    this.vY = 0;
    this.a = 450;
    this.flapA = -350;
  }
  move(time) {
    this.y += (this.vY * time) + (this.a * time * time) / 2;
    this.vY += this.a * time;
  }
  flap() {
    if (this.dead) {
      return;
    }
    this.vY += this.flapA;
    if (this.vY < this.flapA) {
      this.vY = this.flapA;
    }
    this.gotoAndPlay('flap');
  }
  die() {
    this.dead = true;
    this.gotoAndStop('dead');
    this.rotation = 20;
  }
}
