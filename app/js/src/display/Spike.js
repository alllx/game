import assetsManager from '../managers/assetsManager';

export default class Spike extends createjs.Bitmap {
  constructor(queue) {
    super(assetsManager.getResult('spike'));

    this.bounds = this.getBounds();
    this.regY = this.bounds.height;
    this.regX = this.bounds.width / 2;
  }
}
